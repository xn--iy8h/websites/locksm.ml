# Hosting on you own domain from GITlab.



read also [1](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/)


### set up :

- DNS record: set to "your.domain.org A 35.185.44.232"
                  or "your.domain.org CNAME gituser.gitlab.io"
- Gitlab Project Settings > Pages > New Domain
- put a gitlab verification code as a TXT record
   ``_gitlab-pages-verification-code.locksm.ml TXT gitlab-pages-verification-code=c15cc051bd1c9e8d9fa7c3eccce49f85``

